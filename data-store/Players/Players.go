package Players

import (
	"database/sql"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/common-models/Player"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "players"
	return t
}

// FieldsAndVars ...
type FieldsAndVars struct {
	Fields string
	Vars   string
}

func (t *Table) getSelectFieldsAndVars() FieldsAndVars {
	fieldsAndVars := FieldsAndVars{}
	fieldsAndVars.Fields = `id, username, wins, losses, added_at`
	fieldsAndVars.Vars = `?, ?, ?, ?, ?`
	return fieldsAndVars
}

// GetAll ...
func (t *Table) GetAll() ([]Player.Model, error) {
	var players []Player.Model
	var err error

	fieldsAndVars := t.getSelectFieldsAndVars()
	q := `SELECT ` + fieldsAndVars.Fields + ` FROM ` + t.Name

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return players, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]Player.Model, error) {
	var players []Player.Model

	for rows.Next() {
		player := Player.Model{}
		err := rows.Scan(
			&player.ID,
			&player.Username,
			&player.Wins,
			&player.Losses,
			&player.AddedAt,
		)
		if err != nil {
			rows.Close()
			return players, err
		}
		players = append(players, player)
	}

	rows.Close()
	return players, nil
}

// GetAllAvailablePlayers ...
func (t *Table) GetAllAvailablePlayers() {

}
