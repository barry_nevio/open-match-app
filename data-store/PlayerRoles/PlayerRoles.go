package PlayerRoles

import (
	"database/sql"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/common-models/PlayerRole"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "player_roles"
	return t
}

// FieldsAndVars ...
type FieldsAndVars struct {
	Fields string
	Vars   string
}

func (t *Table) getSelectFieldsAndVars() FieldsAndVars {
	fieldsAndVars := FieldsAndVars{}
	fieldsAndVars.Fields = `id, name, added_at`
	fieldsAndVars.Vars = `?, ?, ?`
	return fieldsAndVars
}

// GetAll ...
func (t *Table) GetAll() ([]PlayerRole.Model, error) {
	var playerRoles []PlayerRole.Model
	var err error

	fieldsAndVars := t.getSelectFieldsAndVars()
	q := `SELECT ` + fieldsAndVars.Fields + ` FROM ` + t.Name

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return playerRoles, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]PlayerRole.Model, error) {
	var playerRoles []PlayerRole.Model

	for rows.Next() {
		playerRole := PlayerRole.Model{}
		err := rows.Scan(
			&playerRole.ID,
			&playerRole.Name,
			&playerRole.AddedAt,
		)
		if err != nil {
			rows.Close()
			return playerRoles, err
		}
		playerRoles = append(playerRoles, playerRole)
	}

	rows.Close()
	return playerRoles, nil
}
