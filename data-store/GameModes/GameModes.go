package GameModes

import (
	"database/sql"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/common-models/GameMode"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "game_modes"
	return t
}

// FieldsAndVars ...
type FieldsAndVars struct {
	Fields string
	Vars   string
}

func (t *Table) getSelectFieldsAndVars() FieldsAndVars {
	fieldsAndVars := FieldsAndVars{}
	fieldsAndVars.Fields = `id, name, vanity_name, supported_server_ver, added_at`
	fieldsAndVars.Vars = `?, ?, ?, ?, ?`
	return fieldsAndVars
}

// GetAll ...
func (t *Table) GetAll() ([]GameMode.Model, error) {
	var gameModes []GameMode.Model
	var err error

	fieldsAndVars := t.getSelectFieldsAndVars()
	q := `SELECT ` + fieldsAndVars.Fields + ` FROM ` + t.Name

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return gameModes, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]GameMode.Model, error) {
	var gameModes []GameMode.Model

	for rows.Next() {
		gameMode := GameMode.Model{}
		err := rows.Scan(
			&gameMode.ID,
			&gameMode.Name,
			&gameMode.VanityName,
			&gameMode.SupportedServerVer,
			&gameMode.AddedAt,
		)
		if err != nil {
			rows.Close()
			return gameModes, err
		}
		gameModes = append(gameModes, gameMode)
	}

	rows.Close()
	return gameModes, nil
}
