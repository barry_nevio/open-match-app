package PlayerMatches

import (
	"database/sql"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/common-models/Match"
	"gitlab.com/barry_nevio/open-match-app/common-models/PlayerMatch"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "player_matches"
	return t
}

// FieldsAndVars ...
type FieldsAndVars struct {
	Fields string
	Vars   string
}

// getSelectFieldsAndVars ...
func (t *Table) getSelectFieldsAndVars() FieldsAndVars {
	fieldsAndVars := FieldsAndVars{}
	fieldsAndVars.Fields = `id, player, match, ticket_id, player_role, state, added_at`
	fieldsAndVars.Vars = `?, ?, ?, ?, ?, ?, ?`
	return fieldsAndVars
}

// Add ..
func (t *Table) Add(m Match.Model) (Match.Model, error) {

	return m, nil
}

// GetAll ...
func (t *Table) GetAll() ([]PlayerMatch.Model, error) {
	var playerMatches []PlayerMatch.Model
	var err error

	q := `SELECT ` +
		`player_matches.id, ` +
		`player_matches.ticket_id, ` +
		`player_matches.state, ` +
		`player_matches.added_at, ` +
		`players.id, ` +
		`players.username, ` +
		`players.wins, ` +
		`players.losses, ` +
		`players.added_at, ` +
		`matches.id, ` +
		`matches.state, ` +
		`matches.started_at, ` +
		`matches.ended_at, ` +
		`matches.added_at, ` +
		`servers.id, ` +
		`servers.name, ` +
		`servers.software_ver, ` +
		`servers.state, ` +
		`servers.ip, ` +
		`servers.port, ` +
		`servers.added_at, ` +
		`game_modes.id, ` +
		`game_modes.name, ` +
		`game_modes.vanity_name, ` +
		`game_modes.supported_server_ver, ` +
		`game_modes.added_at, ` +
		`player_roles.id, ` +
		`player_roles.name, ` +
		`player_roles.added_at ` +
		`FROM player_matches LEFT JOIN players ` +
		`ON player_matches.player_id = players.id ` +
		`LEFT JOIN matches ` +
		`ON player_matches.match_id = matches.id ` +
		`LEFT JOIN servers ` +
		`ON matches.server_id = servers.id ` +
		`LEFT JOIN game_modes ` +
		`ON matches.game_mode_id = game_modes.id ` +
		`LEFT JOIN player_roles ` +
		`ON player_matches.player_role_id = player_roles.id `

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return playerMatches, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]PlayerMatch.Model, error) {
	var playerMatches []PlayerMatch.Model

	for rows.Next() {

		// Use a rough
		playerMatchRough := PlayerMatch.Rough{}

		err := rows.Scan(
			&playerMatchRough.ID,
			&playerMatchRough.TicketID,
			&playerMatchRough.State,
			&playerMatchRough.AddedAt,
			&playerMatchRough.Player.ID,
			&playerMatchRough.Player.Username,
			&playerMatchRough.Player.Wins,
			&playerMatchRough.Player.Losses,
			&playerMatchRough.Player.AddedAt,
			&playerMatchRough.Match.ID,
			&playerMatchRough.Match.State,
			&playerMatchRough.Match.StartedAt,
			&playerMatchRough.Match.EndedAt,
			&playerMatchRough.Match.AddedAt,
			&playerMatchRough.Match.Server.ID,
			&playerMatchRough.Match.Server.Name,
			&playerMatchRough.Match.Server.SoftwareVer,
			&playerMatchRough.Match.Server.State,
			&playerMatchRough.Match.Server.IP,
			&playerMatchRough.Match.Server.Port,
			&playerMatchRough.Match.Server.AddedAt,
			&playerMatchRough.Match.GameMode.ID,
			&playerMatchRough.Match.GameMode.Name,
			&playerMatchRough.Match.GameMode.VanityName,
			&playerMatchRough.Match.GameMode.SupportedServerVer,
			&playerMatchRough.Match.GameMode.AddedAt,
			&playerMatchRough.PlayerRole.ID,
			&playerMatchRough.PlayerRole.Name,
			&playerMatchRough.PlayerRole.AddedAt,
		)
		if err != nil {
			rows.Close()
			return playerMatches, err
		}

		// Append refined match
		playerMatches = append(playerMatches, playerMatchRough.Refine())

	}

	rows.Close()
	return playerMatches, nil
}
