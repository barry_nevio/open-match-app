package Matches

import (
	"database/sql"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/common-models/Match"
)

// Table ...
type Table struct {
	DBConn *Database.Connection
	Name   string
}

// New ...
func New(dbConn *Database.Connection) Table {
	t := Table{}
	t.DBConn = dbConn
	t.Name = "matches"
	return t
}

// FieldsAndVars ...
type FieldsAndVars struct {
	Fields string
	Vars   string
}

// getSelectFieldsAndVars ...
func (t *Table) getSelectFieldsAndVars() FieldsAndVars {
	fieldsAndVars := FieldsAndVars{}
	fieldsAndVars.Fields = `id, name, added_at`
	fieldsAndVars.Vars = `?, ?, ?`
	return fieldsAndVars
}

// Add ..
func (t *Table) Add(m Match.Model) (Match.Model, error) {

	return m, nil
}

// GetAll ...
func (t *Table) GetAll() ([]Match.Model, error) {
	var matches []Match.Model
	var err error

	q := `SELECT ` +
		`matches.id, ` +
		`matches.state, ` +
		`matches.started_at, ` +
		`matches.ended_at, ` +
		`matches.added_at, ` +
		`servers.id, ` +
		`servers.name, ` +
		`servers.software_ver, ` +
		`servers.state, ` +
		`servers.ip, ` +
		`servers.port, ` +
		`servers.added_at, ` +
		`game_modes.id, ` +
		`game_modes.name, ` +
		`game_modes.vanity_name, ` +
		`game_modes.supported_server_ver, ` +
		`game_modes.added_at ` +
		`FROM matches LEFT JOIN servers ` +
		`ON matches.server_id = servers.id ` +
		`LEFT JOIN game_modes ` +
		`ON matches.game_mode_id = game_modes.id `

	db, err := t.DBConn.Get()
	if err != nil {
		return nil, err
	}

	rows, err := db.Query(q)
	if err != nil {
		return matches, err
	}

	return t.ProcessSelectQuery(rows)
}

// ProcessSelectQuery ...
func (t *Table) ProcessSelectQuery(rows *sql.Rows) ([]Match.Model, error) {
	var matches []Match.Model

	for rows.Next() {

		// Use a rough
		matchRough := Match.Rough{}

		err := rows.Scan(
			&matchRough.ID,
			&matchRough.State,
			&matchRough.StartedAt,
			&matchRough.EndedAt,
			&matchRough.AddedAt,
			&matchRough.Server.ID,
			&matchRough.Server.Name,
			&matchRough.Server.SoftwareVer,
			&matchRough.Server.State,
			&matchRough.Server.IP,
			&matchRough.Server.Port,
			&matchRough.Server.AddedAt,
			&matchRough.GameMode.ID,
			&matchRough.GameMode.Name,
			&matchRough.GameMode.VanityName,
			&matchRough.GameMode.SupportedServerVer,
			&matchRough.GameMode.AddedAt,
		)
		if err != nil {
			rows.Close()
			return matches, err
		}

		// Append refined match
		matches = append(matches, matchRough.Refine())

	}

	rows.Close()
	return matches, nil
}
