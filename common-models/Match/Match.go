package Match

import (
	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/open-match-app/common-models/GameMode"
	"gitlab.com/barry_nevio/open-match-app/common-models/Server"
)

// Model ...
type Model struct {
	ID        int64          `json:"id"`
	Server    Server.Model   `json:"server"`
	GameMode  GameMode.Model `json:"game_mode"`
	State     string         `json:"state"`
	StartedAt string         `json:"started_at"`
	EndedAt   string         `json:"ended_at"`
	AddedAt   string         `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID        interface{}    `json:"id"`
	Server    Server.Rough   `json:"server"`
	GameMode  GameMode.Rough `json:"game_mode"`
	State     interface{}    `json:"state"`
	StartedAt interface{}    `json:"started_at"`
	EndedAt   interface{}    `json:"ended_at"`
	AddedAt   interface{}    `json:"added_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Server = r.Server.Refine()
	m.GameMode = r.GameMode.Refine()
	m.State = cast.ToString(r.State)
	m.StartedAt = cast.ToString(r.StartedAt)
	m.EndedAt = cast.ToString(r.EndedAt)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
