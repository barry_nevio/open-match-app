package Server

import "github.com/spf13/cast"

// Model ...
type Model struct {
	ID          int64  `json:"id"`
	Name        string `json:"name"`
	SoftwareVer string `json:"software_ver"`
	State       string `json:"state"`
	IP          string `json:"ip"`
	Port        string `json:"port"`
	AddedAt     string `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID          interface{} `json:"id"`
	Name        interface{} `json:"name"`
	SoftwareVer interface{} `json:"software_ver"`
	State       interface{} `json:"state"`
	IP          interface{} `json:"ip"`
	Port        interface{} `json:"port"`
	AddedAt     interface{} `json:"added_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Name = cast.ToString(r.Name)
	m.SoftwareVer = cast.ToString(r.SoftwareVer)
	m.State = cast.ToString(r.State)
	m.IP = cast.ToString(r.IP)
	m.Port = cast.ToString(r.Port)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
