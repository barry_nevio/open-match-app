package Player

import "github.com/spf13/cast"

// Model ...
type Model struct {
	ID       int64  `json:"id"`
	Username string `json:"username"`
	Wins     int    `json:"wins"`
	Losses   int    `json:"losses"`
	AddedAt  string `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID       interface{} `json:"id"`
	Username interface{} `json:"username"`
	Wins     interface{} `json:"wins"`
	Losses   interface{} `json:"losses"`
	AddedAt  interface{} `json:"added_at"`
}

// GetSkill ...
func (m *Model) GetSkill() float64 {

	// Total matches
	total := m.Wins + m.Losses

	// Avoid doing math on 0
	if m.Wins == 0 || total == 0 {
		return 0
	}

	// Avoid doing sure math
	if total == m.Wins {
		return 100
	}

	return (float64(m.Wins) * float64(100)) / float64(total)

}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Username = cast.ToString(r.Username)
	m.Wins = cast.ToInt(r.Wins)
	m.Losses = cast.ToInt(r.Losses)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
