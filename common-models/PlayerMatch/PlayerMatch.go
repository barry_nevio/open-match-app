package PlayerMatch

import (
	"github.com/spf13/cast"
	"gitlab.com/barry_nevio/open-match-app/common-models/Match"
	"gitlab.com/barry_nevio/open-match-app/common-models/Player"
	"gitlab.com/barry_nevio/open-match-app/common-models/PlayerRole"
)

// Model ...
type Model struct {
	ID         int64            `json:"id"`
	Player     Player.Model     `json:"player"`
	Match      Match.Model      `json:"match"`
	TicketID   string           `json:"ticket_id"`
	PlayerRole PlayerRole.Model `json:"player_role"`
	State      string           `json:"state"`
	AddedAt    string           `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID         interface{}      `json:"id"`
	Player     Player.Rough     `json:"player"`
	Match      Match.Rough      `json:"match"`
	TicketID   interface{}      `json:"ticket_id"`
	PlayerRole PlayerRole.Rough `json:"player_role"`
	State      interface{}      `json:"state"`
	AddedAt    interface{}      `json:"added_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Player = r.Player.Refine()
	m.Match = r.Match.Refine()
	m.TicketID = cast.ToString(r.TicketID)
	m.PlayerRole = r.PlayerRole.Refine()
	m.State = cast.ToString(r.State)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
