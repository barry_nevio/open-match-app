package PlayerRole

import "github.com/spf13/cast"

type Model struct {
	ID      int64  `json:"id"`
	Name    string `json:"name"`
	AddedAt string `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID      interface{} `json:"id"`
	Name    interface{} `json:"name"`
	AddedAt interface{} `json:"added_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Name = cast.ToString(r.Name)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
