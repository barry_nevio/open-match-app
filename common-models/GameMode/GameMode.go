package GameMode

import "github.com/spf13/cast"

// Model ...
type Model struct {
	ID                 int64  `json:"id"`
	Name               string `json:"name"`
	VanityName         string `json:"vanity_name"`
	SupportedServerVer string `json:"supported_server_ver"`
	AddedAt            string `json:"added_at"`
}

// Rough ...
type Rough struct {
	ID                 interface{} `json:"id"`
	Name               interface{} `json:"name"`
	VanityName         interface{} `json:"vanity_name"`
	SupportedServerVer interface{} `json:"supported_server_ver"`
	AddedAt            interface{} `json:"added_at"`
}

// Refine ...
func (r *Rough) Refine() Model {
	m := Model{}
	m.ID = cast.ToInt64(r.ID)
	m.Name = cast.ToString(r.Name)
	m.VanityName = cast.ToString(r.VanityName)
	m.SupportedServerVer = cast.ToString(r.SupportedServerVer)
	m.AddedAt = cast.ToString(r.AddedAt)
	return m
}
