package GetTicket

import (
	"context"

	"gitlab.com/barry_nevio/open-match-app/frontend/apps/GetTicket"
	"gitlab.com/barry_nevio/open-match-app/frontend/lib/Error"
	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/matchfunction"
	"open-match.dev/open-match/pkg/pb"
)

type Controller struct {
	App      *GetTicket.App
	GrpcConn *grpc.ClientConn
}

func Init(app *GetTicket.App) Controller {
	c := Controller{}
	c.App = app
	return c
}

func (c *Controller) Process() *GetTicket.App {

	// Only get one ticket
	if !c.App.Payload.GetAll {
		c.getTicket()
		return c.App
	}

	// Get all tickets
	if c.App.Payload.GetAll {
		c.getAllTickets()
		return c.App
	}

	return c.App
}

// getTicket ...
func (c *Controller) getTicket() {
	// Init an error
	var err error

	// Open gRPC Connection to Open Match CORE Frontend.
	c.GrpcConn, err = grpc.Dial(
		c.App.Settings.OpenMatchCore.FrontEnd.Address+":"+c.App.Settings.OpenMatchCore.FrontEnd.Port,
		grpc.WithInsecure(),
	)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT CONNECT TO OPEN MATCH CORE FRONT END VIA gRPC. REASON: " + err.Error())
		c.GrpcConn.Close()
		return
	}
	defer c.GrpcConn.Close()

	// Connect To Open Match CORE Front End Client
	fe := pb.NewFrontendClient(c.GrpcConn)

	// Generate Get Ticket Request
	req := &pb.GetTicketRequest{
		TicketId: c.App.Payload.TicketID,
	}

	// Get Ticket And Handle Error
	resp, err := fe.GetTicket(context.Background(), req)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("FAILED TO GET TICKET. REASON: " + err.Error())
		c.GrpcConn.Close()
		return
	}

	// Success, add to view
	c.App.View.TicketIDs = append(c.App.View.TicketIDs, resp.Id)
}

// getAllTickets ...
func (c *Controller) getAllTickets() {
	// Init an error
	var err error

	// Open gRPC Connection to Open Match CORE Match Making logic.
	c.GrpcConn, err = grpc.Dial(
		c.App.Settings.OpenMatchCore.MML.Address+":"+c.App.Settings.OpenMatchCore.MML.Port,
		grpc.WithInsecure(),
	)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT CONNECT TO OPEN MATCH CORE MATCH MAKING LOGIC VIA gRPC. REASON: " + err.Error())
		c.GrpcConn.Close()
		return
	}
	defer c.GrpcConn.Close()

	// Empty profile query to get all tickets
	emptyReq := pb.RunRequest{}

	poolTickets, err := matchfunction.QueryPools(context.Background(), pb.NewMmLogicClient(c.GrpcConn), emptyReq.GetProfile().GetPools())
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT QUERY MATCH FUNCTION POOLS. REASON: " + err.Error())
		c.GrpcConn.Close()
		return
	}

	for _, pool := range poolTickets {
		for _, ticket := range pool {
			c.App.View.TicketIDs = append(c.App.View.TicketIDs, ticket.Id)
		}
	}

	return

}
