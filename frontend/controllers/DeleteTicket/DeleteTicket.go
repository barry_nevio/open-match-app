package DeleteTicket

import (
	"context"

	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"

	"gitlab.com/barry_nevio/open-match-app/frontend/apps/DeleteTicket"
	"gitlab.com/barry_nevio/open-match-app/frontend/lib/Error"
)

type Controller struct {
	App      *DeleteTicket.App
	GrpcConn *grpc.ClientConn
}

func Init(app *DeleteTicket.App) Controller {
	c := Controller{}
	c.App = app
	return c
}

func (c *Controller) Process() *DeleteTicket.App {

	// Init an error
	var err error

	// Open gRPC Connection to Open Match CORE Frontend.
	c.GrpcConn, err = grpc.Dial(
		c.App.Settings.OpenMatchCore.FrontEnd.Address+":"+c.App.Settings.OpenMatchCore.FrontEnd.Port,
		grpc.WithInsecure(),
	)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT CONNECT TO OPEN MATCH CORE FRONT END VIA gRPC. REASON: " + err.Error())
		c.GrpcConn.Close()
		return c.App
	}
	defer c.GrpcConn.Close()

	// Just delete the tickets in the array
	if !c.App.Payload.DeleteAll {
		err = c.DeleteTickets(c.App.Payload.TicketIDs)
		if err != nil {
			c.App.State.ErrorModel.Err = Error.New("FAILED TO DELETE TICKET. REASON: " + err.Error())
			c.GrpcConn.Close()
			return c.App
		}
	}

	// Delete ALL tickets
	if c.App.Payload.DeleteAll {
		c.App.State.ErrorModel.Err = Error.New("umm, yeah, i didn't program the abiliy to delete all tickets yet")
		c.GrpcConn.Close()
		return c.App
	}

	// Success, add to view
	c.App.View.Message = "SUCCESS!"

	return c.App
}

func (c *Controller) DeleteTickets(ticketIDs []string) error {

	// Connect To Open Match CORE Front End Client
	fe := pb.NewFrontendClient(c.GrpcConn)

	for _, ticketID := range ticketIDs {
		// Generate Delete Ticket Request
		req := &pb.DeleteTicketRequest{
			TicketId: ticketID,
		}
		// Create Ticket And Handle Error
		_, err := fe.DeleteTicket(context.Background(), req)
		if err != nil {
			return err
		}
	}

	return nil
}
