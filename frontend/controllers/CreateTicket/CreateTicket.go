package CreateTicket

import (
	"context"

	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"

	"gitlab.com/barry_nevio/open-match-app/frontend/apps/CreateTicket"
	"gitlab.com/barry_nevio/open-match-app/frontend/lib/Error"
)

type Controller struct {
	App      *CreateTicket.App
	GrpcConn *grpc.ClientConn
}

func Init(app *CreateTicket.App) Controller {
	c := Controller{}
	c.App = app
	return c
}

func (c *Controller) Process() *CreateTicket.App {

	// Init an error
	var err error

	// Open gRPC Connection to Open Match CORE Frontend.
	c.GrpcConn, err = grpc.Dial(
		c.App.Settings.OpenMatchCore.FrontEnd.Address+":"+c.App.Settings.OpenMatchCore.FrontEnd.Port,
		grpc.WithInsecure(),
	)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT CONNECT TO OPEN MATCH CORE FRONT END VIA gRPC. REASON: " + err.Error())
		c.GrpcConn.Close()
		return c.App
	}
	defer c.GrpcConn.Close()

	// Connect To Open Match CORE Front End Client
	fe := pb.NewFrontendClient(c.GrpcConn)

	// Generate New Ticket Request
	req := &pb.CreateTicketRequest{
		Ticket: c.makeTicket(),
	}

	// Create Ticket And Handle Error
	resp, err := fe.CreateTicket(context.Background(), req)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("FAILED TO CREATE TICKET. REASON: " + err.Error())
		c.GrpcConn.Close()
		return c.App
	}

	// Success, add to view
	c.App.View.TicketID = resp.Ticket.Id

	return c.App
}

func (c *Controller) makeTicket() *pb.Ticket {
	ticket := &pb.Ticket{
		SearchFields: &pb.SearchFields{
			Tags: []string{
				c.App.Payload.GameMode,
			},
			StringArgs: map[string]string{
				"attributes.role": c.App.Payload.Role,
			},
		},
	}

	return ticket
}

