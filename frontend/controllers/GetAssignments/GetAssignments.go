package GetAssignments

import (
	"context"

	"github.com/spf13/cast"
	"google.golang.org/grpc"
	"open-match.dev/open-match/pkg/pb"

	"gitlab.com/barry_nevio/open-match-app/frontend/apps/GetAssignments"
	"gitlab.com/barry_nevio/open-match-app/frontend/lib/Error"
)

type Controller struct {
	App      *GetAssignments.App
	GrpcConn *grpc.ClientConn
}

func Init(app *GetAssignments.App) Controller {
	c := Controller{}
	c.App = app
	return c
}

func (c *Controller) Process() *GetAssignments.App {

	// Init an error
	var err error

	// Open gRPC Connection to Open Match CORE Frontend.
	c.GrpcConn, err = grpc.Dial(
		c.App.Settings.OpenMatchCore.FrontEnd.Address+":"+c.App.Settings.OpenMatchCore.FrontEnd.Port,
		grpc.WithInsecure(),
	)
	if err != nil {
		c.App.State.ErrorModel.Err = Error.New("CAN NOT CONNECT TO OPEN MATCH CORE FRONT END VIA gRPC. REASON: " + err.Error())
		c.GrpcConn.Close()
		return c.App
	}
	defer c.GrpcConn.Close()

	// Connect To Open Match CORE Front End Client
	fe := pb.NewFrontendClient(c.GrpcConn)

	// Generate Get Assignment Request
	/*req := &pb.GetAssignmentsRequest{
		TicketId: c.App.Payload.TicketID,
	}*/

	// Check ticket for assignment
	for {

		got, err := fe.GetTicket(context.Background(), &pb.GetTicketRequest{TicketId: c.App.Payload.TicketID})
		if err != nil {
			c.App.State.ErrorModel.Err = Error.New("FAILED TO GET TICKET. REASON: " + err.Error())
			c.GrpcConn.Close()
			return c.App
		}

		if got.GetAssignment() != nil {

			/*resp, err := getAssignmentsClient.Recv()
			if err != nil {
				c.App.State.ErrorModel.Err = Error.New("FAILED TO GET ASSIGNMENT RESPONSE. REASON: " + err.Error())
				c.GrpcConn.Close()
				return c.App
			}*/

			// Success, add to view
			//c.App.View.Connection = resp.Assignment.Connection
			c.App.View.Assignment = cast.ToString(got.GetAssignment())
			c.App.View.Assigned = true
			break
		}
	}

	return c.App
}
