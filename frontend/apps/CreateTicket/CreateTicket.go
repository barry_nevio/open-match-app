package CreateTicket

import (
	"gitlab.com/barry_nevio/open-match-app/frontend/models/Error"
	"gitlab.com/barry_nevio/open-match-app/frontend/models/Settings"
	Payload "gitlab.com/barry_nevio/open-match-app/frontend/models/payloads/CreateTicket"
	View "gitlab.com/barry_nevio/open-match-app/frontend/views/CreateTicket"
)

type App struct {
	Payload Payload.Payload
	State   struct {
		ErrorModel Error.Model
	}
	View     View.View
	Settings Settings.Settings
}

// New ... Nothing fancy, just a new App struct
func New() App {
	a := App{}
	a.State.ErrorModel = Error.NewFromErr(nil)
	a.Settings = Settings.New()
	a.View = View.New()
	return a
}

func NewFromPayloadBytes(payloadBytes []byte) App {
	a := New()
	a.Payload = Payload.NewFromBytes(payloadBytes)
	// Add the request payload to the view for better visual
	a.View.RequestPayload = a.Payload
	return a
}
