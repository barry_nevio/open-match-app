package Request

import (
	"github.com/gin-gonic/gin"
	CreateTicketApp "gitlab.com/barry_nevio/open-match-app/frontend/apps/CreateTicket"
	DeleteTicketApp "gitlab.com/barry_nevio/open-match-app/frontend/apps/DeleteTicket"
	GetAssignmentsApp "gitlab.com/barry_nevio/open-match-app/frontend/apps/GetAssignments"
	GetTicketApp "gitlab.com/barry_nevio/open-match-app/frontend/apps/GetTicket"
	CreateTicketController "gitlab.com/barry_nevio/open-match-app/frontend/controllers/CreateTicket"
	DeleteTicketController "gitlab.com/barry_nevio/open-match-app/frontend/controllers/DeleteTicket"
	GetAssignmentsController "gitlab.com/barry_nevio/open-match-app/frontend/controllers/GetAssignments"
	GetTicketController "gitlab.com/barry_nevio/open-match-app/frontend/controllers/GetTicket"
)

func Handle(forApp string, ginCtx *gin.Context) (int, map[string]interface{}) {

	// Get the request body
	bodyBytes, _ := ginCtx.GetRawData()

	// Headers if ya need em
	//ginCtx.Request.Header

	switch forApp {
	case "CREATE_TICKET":
		createTicketApp := CreateTicketApp.NewFromPayloadBytes(bodyBytes)
		createTicketController := CreateTicketController.Init(&createTicketApp)
		createTicketController.Process()

		// If there was an error, return the error view instead
		if createTicketApp.State.ErrorModel.Err != nil {
			errorView := createTicketApp.State.ErrorModel.GetView()
			return errorView.HTTPStatusCode, errorView.GetAsMap()
		}
		return createTicketApp.View.HTTPStatusCode, createTicketApp.View.GetAsMap()

	case "GET_TICKET":
		getTicketApp := GetTicketApp.NewFromPayloadBytes(bodyBytes)
		getTicketController := GetTicketController.Init(&getTicketApp)
		getTicketController.Process()

		// If there was an error, return the error view instead
		if getTicketApp.State.ErrorModel.Err != nil {
			errorView := getTicketApp.State.ErrorModel.GetView()
			return errorView.HTTPStatusCode, errorView.GetAsMap()
		}
		return getTicketApp.View.HTTPStatusCode, getTicketApp.View.GetAsMap()

	case "DELETE_TICKET":
		deleteTicketApp := DeleteTicketApp.NewFromPayloadBytes(bodyBytes)
		deleteTicketController := DeleteTicketController.Init(&deleteTicketApp)
		deleteTicketController.Process()

		// If there was an error, return the error view instead
		if deleteTicketApp.State.ErrorModel.Err != nil {
			errorView := deleteTicketApp.State.ErrorModel.GetView()
			return errorView.HTTPStatusCode, errorView.GetAsMap()
		}
		return deleteTicketApp.View.HTTPStatusCode, deleteTicketApp.View.GetAsMap()

	case "GET_ASSIGNMENTS":
		getAssignmentsApp := GetAssignmentsApp.NewFromPayloadBytes(bodyBytes)
		getAssignmentsController := GetAssignmentsController.Init(&getAssignmentsApp)
		getAssignmentsController.Process()

		// If there was an error, return the error view instead
		if getAssignmentsApp.State.ErrorModel.Err != nil {
			errorView := getAssignmentsApp.State.ErrorModel.GetView()
			return errorView.HTTPStatusCode, errorView.GetAsMap()
		}
		return getAssignmentsApp.View.HTTPStatusCode, getAssignmentsApp.View.GetAsMap()

	}

	return 404, make(map[string]interface{})
}
