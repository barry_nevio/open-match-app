package Settings

import (
	"gitlab.com/barry_nevio/open-match-app/frontend/models/Settings/OpenMatchCore"
)

type Settings struct {
	OpenMatchCore OpenMatchCore.Settings
}

// New ... Returns model of all settings and their default values
func New() Settings {
	s := Settings{}
	s.OpenMatchCore = OpenMatchCore.New()
	return s
}
