package OpenMatchCore

type ServiceInfo struct {
	Address string
	Port    string
}

type Settings struct {
	FrontEnd ServiceInfo
	BackEnd  ServiceInfo
	MMF      ServiceInfo
	MML      ServiceInfo
}

// New ... Sets default settings and returns the struct
func New() Settings {
	s := Settings{}
	s.FrontEnd.Address = "om-frontend.open-match.svc.cluster.local"
	s.FrontEnd.Port = "50504"

	s.BackEnd.Address = "om-backend.open-match.svc.cluster.local"
	s.BackEnd.Port = "50505"

	s.MMF.Address = "om-backend.open-match.svc.cluster.local"
	s.MMF.Port = "50505"

	s.MML.Address = "om-mmlogic.open-match.svc.cluster.local"
	s.MML.Port = "50503"
	return s
}
