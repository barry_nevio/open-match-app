package CreateTicket

import (
	"encoding/json"

	"github.com/spf13/cast"
)

type Payload struct {
	GameMode string `json:"game_mode"`
	Players  int    `json:"players"`
	Latency  int    `json:"latency"`
	MMR      int    `json:"mmr"`
	Role     string `json:"role"`
}

type RoughPayload struct {
	GameMode interface{} `json:"game_mode"`
	Players  interface{} `json:"players"`
	Latency  interface{} `json:"latency"`
	MMR      interface{} `json:"mmr"`
	Role     interface{} `json:"role"`
}

// New ... Nothing special, just an empty payload
func New() Payload {
	p := Payload{}
	return p
}

// NewFromBytes ... Take in the json bytes to get a filled in payload
func NewFromBytes(bytes []byte) Payload {
	p := Payload{}
	roughP := RoughPayload{}

	// Unmarshall into rough payload
	// ignore error, if you can't send this the right data, you are a moron
	_ = json.Unmarshal(bytes, &roughP)

	// Cast to correct data types
	p.GameMode = cast.ToString(roughP.GameMode)
	p.Players = cast.ToInt(roughP.Players)
	p.Latency = cast.ToInt(roughP.Latency)
	p.MMR = cast.ToInt(roughP.MMR)
	p.Role = cast.ToString(roughP.Role)

	// Set default values
	if len(p.Role) < 1 {
		p.Role = "dps"
	}

	return p
}
