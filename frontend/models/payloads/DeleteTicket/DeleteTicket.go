package DeleteTicket

import (
	"encoding/json"

	"github.com/spf13/cast"
)

type Payload struct {
	DeleteAll bool     `json:"delete_all"`
	TicketIDs []string `json:"ticket_ids"`
}

type RoughPayload struct {
	DeleteAll interface{}   `json:"delete_all"`
	TicketIDs []interface{} `json:"ticket_ids"`
}

// New ... Nothing special, just an empty payload
func New() Payload {
	p := Payload{}
	return p
}

// NewFromBytes ... Take in the json bytes to get a filled in payload
func NewFromBytes(bytes []byte) Payload {
	p := Payload{}
	roughP := RoughPayload{}

	// Unmarshall into rough payload
	// ignore error, if you can't send this the right data, you are a moron
	_ = json.Unmarshal(bytes, &roughP)

	// Delete all?
	p.DeleteAll = cast.ToBool(roughP.DeleteAll)

	// Add all ticket ids
	for _, ticketID := range roughP.TicketIDs {
		// Cast to correct data type
		p.TicketIDs = append(p.TicketIDs, cast.ToString(ticketID))
	}

	return p
}
