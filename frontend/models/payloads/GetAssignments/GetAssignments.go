package GetAssignments

import (
	"encoding/json"

	"github.com/spf13/cast"
)

type Payload struct {
	TicketID string `json:"ticket_id"`
}

type RoughPayload struct {
	TicketID interface{} `json:"ticket_id"`
}

// New ... Nothing special, just an empty payload
func New() Payload {
	p := Payload{}
	return p
}

// NewFromBytes ... Take in the json bytes to get a filled in payload
func NewFromBytes(bytes []byte) Payload {
	p := Payload{}
	roughP := RoughPayload{}

	// Unmarshall into rough payload
	// ignore error, if you can't send this the right data, you are a moron
	_ = json.Unmarshal(bytes, &roughP)

	// Cast to correct data types
	p.TicketID = cast.ToString(roughP.TicketID)

	return p
}
