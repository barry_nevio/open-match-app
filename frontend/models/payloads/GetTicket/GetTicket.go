package GetTicket

import (
	"encoding/json"

	"github.com/spf13/cast"
)

type Payload struct {
	GetAll   bool   `json:"get_all"`
	TicketID string `json:"ticket_id"`
}

type RoughPayload struct {
	GetAll   interface{} `json:"get_all"`
	TicketID interface{} `json:"ticket_id"`
}

// New ... Nothing special, just an empty payload
func New() Payload {
	p := Payload{}
	return p
}

// NewFromBytes ... Take in the json bytes to get a filled in payload
func NewFromBytes(bytes []byte) Payload {
	p := Payload{}
	roughP := RoughPayload{}

	// Unmarshall into rough payload
	// ignore error, if you can't send this the right data, you are a moron
	_ = json.Unmarshal(bytes, &roughP)

	// Cast to correct data types
	p.GetAll = cast.ToBool(roughP.GetAll)
	p.TicketID = cast.ToString(roughP.TicketID)

	return p
}
