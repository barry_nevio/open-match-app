package Error

import (
	"gitlab.com/barry_nevio/open-match-app/frontend/lib/Error"
	View "gitlab.com/barry_nevio/open-match-app/frontend/views/Error"
)

type Model struct {
	Err            error
	GRPCconnection bool
	InvalidRequest bool
}

func NewFromErr(err error) Model {
	m := Model{}
	m.Err = err
	return m
}

func NewFromString(errS string) Model {
	m := Model{}
	m.Err = Error.New(errS)
	return m
}

func (m *Model) GetView() View.View {
	v := View.New()
	if m.Err != nil {
		v.Message = m.Err.Error()
	}
	return v
}
