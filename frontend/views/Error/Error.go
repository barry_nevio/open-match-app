package Error

import "encoding/json"

type View struct {
	HTTPStatusCode int    `json:"http_status_code"`
	Message        string `json:"message"`
}

// New ... Nohting fancy, just a new View struct
func New() View {
	v := View{}
	v.HTTPStatusCode = 500
	return v
}

// GetAsMap ... Returns the struct as a map
func (v *View) GetAsMap() map[string]interface{} {
	errMap := make(map[string]interface{})
	bytes, _ := json.Marshal(v)
	_ = json.Unmarshal(bytes, &errMap)
	return errMap
}
