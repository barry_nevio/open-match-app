package CreateTicket

import (
	"encoding/json"
	RequestPayload "gitlab.com/barry_nevio/open-match-app/frontend/models/payloads/CreateTicket"
)

type View struct {
	HTTPStatusCode int                    `json:"http_status_code"`
	TicketID       string                 `json:"ticket_id"`
	RequestPayload RequestPayload.Payload `json:"request_payload"`
}

// New ... Nohting fancy, just a new View struct
func New() View {
	v := View{}
	v.HTTPStatusCode = 200
	return v
}

// GetAsMap ... Returns the struct as a map
func (v *View) GetAsMap() map[string]interface{} {
	errMap := make(map[string]interface{})
	bytes, _ := json.Marshal(v)
	_ = json.Unmarshal(bytes, &errMap)
	return errMap
}
