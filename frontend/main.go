package main

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/barry_nevio/open-match-app/frontend/middleware/Request"
)

func main() {

	// set server mode
	gin.SetMode(gin.DebugMode)

	// Instanciate default gin router
	router := gin.Default()

	// ** FRONTEND CALLS: BEGIN ** //

	// New Ticket
	router.POST("/om-app-frontend/new-ticket", func(c *gin.Context) {
		c.JSON(Request.Handle("CREATE_TICKET", c))
		return
	})

	// Get Ticket ... //! Does not work as expected
	router.POST("/om-app-frontend/get-ticket", func(c *gin.Context) {
		c.JSON(Request.Handle("GET_TICKET", c))
		return
	})

	// Delete Ticket
	router.POST("/om-app-frontend/delete-ticket", func(c *gin.Context) {
		c.JSON(Request.Handle("DELETE_TICKET", c))
		return
	})

	// Get Assignments
	router.POST("/om-app-frontend/get-assignments", func(c *gin.Context) {
		c.JSON(Request.Handle("GET_ASSIGNMENTS", c))
		return
	})

	// Fetch Matches
	router.POST("/om-app-frontend/fetch-matches", func(c *gin.Context) {
		c.JSON(Request.Handle("FETCH_MATCHES", c))
		return
	})

	// ** FRONTEND CALLS: END ** //

	// ** DEMO CALLS: BEGIN ** //

	// ** DEMO CALLS: END ** //

	addr := ":8991"
	router.Run(addr)

}
