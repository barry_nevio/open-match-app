package main

import (
	"gitlab.com/barry_nevio/open-match-app/matchfunction/mmf"
)

// This tutorial implenents a basic Match Function that is hosted in the below
// configured port. You can also configure the Open Match MMLogic service endpoint
// with which the Match Function communicates to query the Tickets.

const (
	mmlogicAddress = "om-mmlogic.open-match.svc.cluster.local:50503" // Address of the MMLogic service endpoint.
	serverPort     = 50502                                           // The port for hosting the Match Function.
)

func main() {
	mmf.Start(mmlogicAddress, serverPort)
}
