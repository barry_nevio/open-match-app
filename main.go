package main

import (
	"fmt"

	"gitlab.com/barry_nevio/goo-tools/Database"
	"gitlab.com/barry_nevio/open-match-app/data-store/GameModes"
	"gitlab.com/barry_nevio/open-match-app/data-store/Matches"
	"gitlab.com/barry_nevio/open-match-app/data-store/PlayerMatches"
	"gitlab.com/barry_nevio/open-match-app/data-store/PlayerRoles"
	"gitlab.com/barry_nevio/open-match-app/data-store/Players"
)

func main() {
	dbSetts := Database.Settings{}
	dbSetts.User = "AlaneoJesterripple"
	dbSetts.Password = "DXTypjqaRQWRNhlX"
	dbSetts.Address = "207.246.123.211"
	dbSetts.Port = "3306"
	dbSetts.DB = "online_game"
	db, err := Database.New(dbSetts)
	if err != nil {
		fmt.Println("CAN NOT CONNECT TO DB: " + err.Error())
		return
	}
	// Test game modes from db
	gameModesTable := GameModes.New(&db)
	gameModes, err := gameModesTable.GetAll()
	if err != nil {
		fmt.Println("CAN NOT GET ALL GAMES MODES: " + err.Error())
		return
	}

	for _, gameMode := range gameModes {
		fmt.Println("GAME MODE: " + gameMode.VanityName + " | " + gameMode.Name)
	}

	// Test player roles from db
	playerRolesTable := PlayerRoles.New(&db)
	playerRoles, err := playerRolesTable.GetAll()
	if err != nil {
		fmt.Println("CAN NOT GET ALL PLAYER ROLES: " + err.Error())
		return
	}

	for _, playerRole := range playerRoles {
		fmt.Println("PLAYER ROLE: " + playerRole.Name)
	}

	// Test players from db
	playersTable := Players.New(&db)
	players, err := playersTable.GetAll()
	if err != nil {
		fmt.Println("CAN NOT GET ALL PLAYERS: " + err.Error())
		return
	}

	for _, player := range players {
		fmt.Println("PLAYER: " + player.Username)
	}

	// Test matches from db
	matchesTable := Matches.New(&db)
	matches, err := matchesTable.GetAll()
	if err != nil {
		fmt.Println("CAN NOT GET ALL MATCHES: " + err.Error())
		return
	}

	for _, match := range matches {
		fmt.Println("MATCH: STATE: " + match.State + " | SERVER NAME: " + match.Server.Name)
	}

	// Test player matches from db
	playerMatchesTable := PlayerMatches.New(&db)
	playerMatches, err := playerMatchesTable.GetAll()
	if err != nil {
		fmt.Println("CAN NOT GET ALL PLAYER MATCHES: " + err.Error())
		return
	}

	for _, playerMatch := range playerMatches {
		fmt.Println("PLAYER MATCH: PLAYER USERNAME: " + playerMatch.Player.Username + " | SERVER NAME: " + playerMatch.Match.Server.Name)
	}

}

func addAllAvailablePlayersToQueue() {

}

func generateMatches() {

}

func joinAllPlayers() {

}

func startMatch() {

}

func endMatch() {

}
